// import 'package:get/get.dart';
// import 'package:telephony/telephony.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'package:get_storage/get_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'salut ya qqn?',
        home: Scaffold(
          backgroundColor: Color.fromARGB(255, 48, 47, 47),
          appBar: AppBar(title: const Text('ALLOOO?')),
          body: Center(
            heightFactor: 1.0,
            child: Column(children: [
              Image.network(
                  "https://helpp-life.fr/wp-content/uploads/2021/04/logohelpplife-typobaseline-354x212-1.png",
                  height: 150,
                  width: 200),
              Image.network(
                  "https://resk-you.fr/wp-content/uploads/2022/10/logo-resk-you.png"),
              Expanded(
                  child: Container(
                      width: double.infinity,
                      height: 1.0,
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(15),
                              topLeft: Radius.circular(15)),
                          color: Color.fromARGB(255, 255, 255, 255)),
                      child: Column(children: [
                        Padding(
                            padding: const EdgeInsets.all(50),
                            child: RichText(
                                text: const TextSpan(
                                    text: "Appelez le ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 24,
                                        color: Colors.black),
                                    children: [
                                  TextSpan(
                                      text: "112",
                                      style: TextStyle(
                                          fontSize: 24,
                                          fontWeight: FontWeight.w700,
                                          color: Color.fromRGBO(250, 0, 0, 1)))
                                ]))),
                        ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: Image.asset(
                              "corin.png",
                              width: 100,
                            )),
                        const Text("Qhorin Exalandru",
                            style: TextStyle(
                                fontWeight: FontWeight.w900, fontSize: 20)),
                        const Text("T-FR-HL1-22-XXXXXX",
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 12)),
                        Container(
                            color: const Color.fromARGB(194, 192, 192, 192),
                            margin: const EdgeInsets.all(20.0),
                            height: 1),
                        const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 60),
                            child: Text(
                              "Groupe sanguin A+, Allergie Amoxicilline, sous immunoSuppresseurs apres greffe de bras bionique",
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w800,
                                color: Color(0xffCC1233),
                              ),
                              textAlign: TextAlign.center,
                            ))
                      ])))
            ]),
          ),
        ));
  }
}
